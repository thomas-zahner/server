const displayResponse = (response) => {
  const fields = document.getElementById('fields');
  const table = document.getElementById('table');

  response.fields.forEach(field => {
    const column = document.createElement('th');
    column.scope = 'col';
    column.textContent = field.name;

    fields.appendChild(column);
  });

  response.rows.forEach(dataRow => {
    const tableRow = document.createElement('tr');

    response.fields.forEach(field => {
      const dataField = document.createElement('td');
      dataField.textContent = dataRow[field.name];
      tableRow.appendChild(dataField);
    });

    table.appendChild(tableRow);
  });
};

window.onload = () => {
  const responseField = document.getElementById('response');

  postRequest('/getAccounts', {})
    .then(response => {
      displayResponse(response);
    });
};
