const postRequest = (url, data) => {
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
    .then(response => {
      if(!response.ok) {
        throw new Error('Error in fetching ' + url);
      }
      return response.json();
    });
};
