'use strict';

const path = require('path');
const express = require('express');
const session = require('express-session');

const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const { Client } = require('pg');
const client = new Client();
client.connect();

const port = 8080;
const host = '0.0.0.0';

const loginFailMessage = 'Invalid username or password';

const app = express();
app.use(express.json());

app.use(express.static(path.join(__dirname, '../public/content/')));

app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: crypto.randomBytes(64).toString('hex'),
  cookie: {
    httpOnly: true,
    sameSite: true,
    maxAge: 1000 * 60 * 60 * 24
  }
}));

app.get('/', (req, res, next) => {
  if(req.session.accountId) {
    client.query(`SELECT username FROM account WHERE id = $1;`, [ req.session.accountId ])
      .then(queryResult => {
        res.send('Hi ' + queryResult.rows[0].username);
        res.end();
      }).catch(e => next(e));
  } else {
    res.redirect('/login.html');
  }
});

app.post('/accountSignup', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if(!username || !password) {
    res.json({error: 'Password and username expected'}).end();
  } else {
    client.query(`SELECT FROM account WHERE username = $1;`, [ username ])
      .then(queryResult => {
        if(queryResult.rowCount == 0) {
          bcrypt.hash(password, 10).then(hash => {
            client.query(`INSERT INTO account (username, password) VALUES ($1, $2);`, [ username, hash ])
              .then(queryResult => {
                res.json({redirect: '/'});
              }).catch(e => next(e));
          }).catch(e => next(e));
        } else {
          res.json({error: 'User already exists'});
        }
      }).catch(e => next(e));
  }
});

app.post('/accountLogin', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if(!username || !password) {
    res.json({error: 'Password and username expected'}).end();
  } else {
    client.query(`SELECT * FROM account WHERE username = $1;`, [ username ])
      .then(queryResult => {
        if(queryResult.rows[0]) {
          const hashedPassword = queryResult.rows[0].password;

          bcrypt.compare(password, hashedPassword).then(match => {
            if(match) {
              req.session.accountId = queryResult.rows[0].id;
              res.json({redirect: '/'});
            } else {
              res.json({error: loginFailMessage}).end();
            }
          }).catch(e => next(e));
        } else {
          res.json({error: loginFailMessage}).end();
        }
      }).catch(e => next(e));
  }
});

app.post('/getAccounts', (req, res, next) => {
  client.query(`SELECT * FROM account;`)
    .then(queryResult => {
      const fields = queryResult.fields.map(field => {
        return {name: field.name, format: field.format};
      });

      res.json({
        rows: queryResult.rows,
        fields: fields
      });
    }).catch(e => next(e));
});

//Error handler
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).end();
});

app.use((req, res, next) => {
  if(req.method === 'GET') {
    res.status(404).sendFile(path.join(__dirname, '../public/404/404.html'));
  } else {
    res.status(404).end();
  }
});

app.listen(port, host);
console.log(`Running on ${host}:${port}`);

process.on('SIGINT', () => {
  console.log(`\nTerminating postgres connection`);
  client.end();
  process.exit();
});
